/*
 * LightController.cpp
 *
 *  Created on: Oct 2, 2019
 *  Author: Lily Lux
 */

#include "LightController.h"
#include "NetworkCommands.h"

using namespace se3910RPi;

LightController::LightController(int gpioPin, CommandQueue* queue, std::string threadname, uint32_t period) : PeriodicTask(threadname, period) {
    this->currentCount = 0;
    this->onOffTransitionCount = 0;
    this->referencequeue = queue;
    this->light = new GPIO(gpioPin, se3910RPi::GPIO::GPIO_OUT, se3910RPi::GPIO::GPIO_LOW);
}

LightController::~LightController() {
    delete this->light;
    delete this-> referencequeue;
}

void LightController::taskMethod() {
    if(this->referencequeue->hasItem()) {
        int command = this->referencequeue->dequeue();
        if (command == LIGHTONCMD) {
        	this->onOffTransitionCount = 20;
        } else if (command == LIGHTONEOFFCMD) {
        	this->onOffTransitionCount = 0;
        } else if (command == LIGHTBLINKCMD) {
            this->onOffTransitionCount = 10;
        }
        this->currentCount = -1;
        this->light->setValue(se3910RPi::GPIO::GPIO_HIGH); //turn light off
    }
    if (this->currentCount < 20) {
        this->currentCount = this->currentCount + 1;
    } else {
        this->currentCount = 0;
    }

    if (this->currentCount < this->onOffTransitionCount) {
        this->light->setValue(se3910RPi::GPIO::GPIO_LOW); //turn light on
    } else {
    	this->light->setValue(se3910RPi::GPIO::GPIO_HIGH); // turn light off
    }
}
