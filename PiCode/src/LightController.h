/*
 * LightController.h
 *
 *  Created on: Oct 2, 2019
 *      Author: se3910
 */
#include "GPIO.h"
#include "CommandQueue.h"
#include "PeriodicTask.h"

#ifndef LIGHTCONTROLLER_H_
#define LIGHTCONTROLLER_H_

class LightController : public PeriodicTask{
private:
	  CommandQueue* referencequeue;
	  se3910RPi::GPIO* light;
      int currentCount;
	  int onOffTransitionCount;
public:
  LightController(int gpioPin, CommandQueue* queue, std::string threadname, uint32_t period);
  virtual ~LightController();
  void taskMethod();
};

#endif /* LIGHTCONTROLLER_H_ */
