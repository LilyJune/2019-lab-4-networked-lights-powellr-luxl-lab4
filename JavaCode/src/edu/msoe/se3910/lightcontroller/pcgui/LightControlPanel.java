package edu.msoe.se3910.lightcontroller.pcgui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;

import edu.msoe.se3910.lightcontroller.core.iNetworkController;

/**
 * This class defines the light control gui.
 * 
 * @author schilling
 *
 */
public class LightControlPanel extends JPanel {
	/**
	 * These are the messages which will be sent to turn a light on or off.
	 */
	static final int LIGHTOFFCMD = 0x00000001;
	static final int LIGHTONCMD = 0x00000010;
	static final int LIGHTBLINKCMD = 0x00000100;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LightControlPanel(final iNetworkController NetworkController) {

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(0, 2));

		BorderLayout myLayout = new BorderLayout();
		this.setLayout(myLayout);

		JButton lightOneOn = new JButton("L1 ON");
		JButton lightOneOff = new JButton("L1 OFF");
		JButton lightOneBlink = new JButton("L1 Blink");
		JButton lightTwoOn = new JButton("L2 ON");
		JButton lightTwoOff = new JButton("L2 OFF");
		JButton lightTwoBlink = new JButton("L2 Blink");

		mainPanel.add(lightOneBlink);
		mainPanel.add(lightTwoBlink);
		mainPanel.add(lightOneOn);
		mainPanel.add(lightTwoOn);
		mainPanel.add(lightOneOff);
		mainPanel.add(lightTwoOff);

		add(mainPanel, BorderLayout.CENTER);

		lightOneOff.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					NetworkController.sendMessage(1, LIGHTOFFCMD);
				} catch (IOException e1) {
				}
			}
		});

		lightOneOn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					NetworkController.sendMessage(1, LIGHTONCMD);
				} catch (IOException e1) {
				}
			}
		});

		lightOneBlink.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					NetworkController.sendMessage(1, LIGHTBLINKCMD);
				} catch (IOException e1) {
				}
			}
		});

		lightTwoOff.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					NetworkController.sendMessage(2, LIGHTOFFCMD);
				} catch (IOException e1) {
				}

			}
		});

		lightTwoOn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					NetworkController.sendMessage(2, LIGHTONCMD);
				} catch (IOException e1) {
				}
			}
		});

		lightTwoBlink.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					NetworkController.sendMessage(2, LIGHTBLINKCMD);
				} catch (IOException e1) {
				}
			}
		});
	}
}
