package edu.msoe.se3910.lightcontroller.core;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 * This interface defines a network controller. The network controller will
 * allow the user to communicate with a single Raspberry PI instance and send
 * commands to it.
 * 
 * @author wws
 *
 */
public interface iNetworkController {
	/**
	 * This method will set the IP address of the raspberry pi.
	 * 
	 * @param ip
	 *            The ip address in the form AAA.BBB.CCC.DDD
	 */
	public void setIPAddress(String ip);

	/**
	 * This method will connect to the given IP address.
	 * 
	 * @throws UnknownHostException
	 *             This will be thrown if no connection can be made with the host.
	 * @throws IOException
	 *             This exception will be thrown if IO can not be connected with.
	 */
	public void connect() throws UnknownHostException, IOException;

	/**
	 * This method will disconnect from the given networked machine.
	 */
	public void disconnect();

	/**
	 * This method will return the connection status.
	 * 
	 * @return true if connected. False OTW.
	 */
	public boolean getConnectionStatus();

	/**
	 * This method will send a single 32 bit message to the remote machine.
	 * 
	 * @param destination
	 *            This is the destination for the given message.
	 * @param message
	 *            This is the message to send.
	 * @throws IOException
	 *             This exception will be thrown if there is an error.
	 */
	void sendMessage(int destination, int message) throws IOException;
}
